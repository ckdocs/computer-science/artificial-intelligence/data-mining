# Data Selection

In this step we learn how to **reduce** data in **dimension** and **numerosity**

There are three types of **reduction**:

1. Dimensionality Reduction (Data Attribute)
2. Numerosity Reduction (Data Object)
3. Compression Reduction (Binary)

<!--prettier-ignore-->
!!! tip "Reasons"
    There are many reasons for reducing data:

    1. Reduce storage
    2. Reduce memory
    3. Reduce time
    4. Increase speed
    5. Clean noise and outlier
    6. Easier visualization

    ---

    There are many statistical reasons for reducing data:

    1. Avoid **multicollinearity**
        * Remove **correlated** features
    2. Increase **degree of freedom**
    3. Avoid **overfitting**
        * Model is not general
        * Traning set was not good
        * Model is only matched to traning set

<!--prettier-ignore-->
!!! tip "How much reduction?"
    We reduce our data until that the **quality** does **not decrease**

<!--prettier-ignore-->
!!! danger "Normalization"
    Before start reducing data, we must normalize our data

    Imagine we have a two features

    1. **A**: Range: **[-1,1]**, so important for us
    2. **B**: Range: **[0,1000]**, not important for us

    By reducing data, feature **A** may be detected as unuseful feature, so before reducing we most normalize all values

    1. **A**: New range: **[-1,1]**
    2. **B**: New range: **[-1,1]**

---

## Dimensionality Reduction

Reducing Data Attributes

<!--prettier-ignore-->
!!! tip "Curse of dimensionality"
    Sometime our DataObjects have a lot of features:
    
    1. Sparce features
    2. Correlated features

    We can remove these types of features to **Reduce Memory** and **Increase Speed**

    ---

    **Example**:

    **Featue**: The number of spots on the face

    A lot of peoples does not have any spot on their faces, so this feature is **Sparce**

### Discrete Wavelet Transform (DWT)

We use **Signal Processing** algorithms to reduce dataset dimension

Discrete wavelet transform will extract useful **features**, then it can remove unuseful features:

1. Convert features
2. Select **K** useful features

<!--prettier-ignore-->
!!! tip "When to DWT?"
    For huge amounts of dimensions DWT will be a good choice such as **Image**, **Video**

---

### Principal Component Analysus (PCA)

We use **Linear Algebra** decomposition algorithms to reduce dataset dimension

Signular value decomposition (SVD) will extract useful **features**, then it can remove unuseful features:

1. Convert features
2. Select **K** useful features

<!--prettier-ignore-->
!!! tip "When to PCA?"
    For small amounts of dimensions PCA will be a good choice such as **Text** (Heavy calculation)

---

### Feature Subset Detection

Is another way to reduce dimensions without converting them:

1. Select **K** useful features
2. Remove **Correlated** features (Age, Birthdate)
3. Remove **Unnecessary** features (UserID, EyeColor)

<!--prettier-ignore-->
!!! tip "Forward Selection"
    Is an algorithm to subset selection:

    1. Start with empty array of features **[]**
    2. Sort features based on **Use** ascending
    3. Iterate features
       1. If **Not Correlated** and **Necessary** add it

    ![Forward Selection](assets/data_mining_feature_selection_forward_selection.png)

<!--prettier-ignore-->
!!! tip "Backward Elimination"
    Is an algorithm to subset selection:

    1. Start with full array of features **[A, B, C, ...]**
    2. Sort features based on **Use** descending
    3. Iterate features
       1. If **Correlated** or **Unnecessary** remove it

    ![Backward Elimination](assets/data_mining_feature_selection_backward_elimination.png)

<!--prettier-ignore-->
!!! tip "Decision Tree"
    Is a simple way to select the useful features based on generated decision tree:

    1. Remove features **not occured** in decision tree

    ![Decision Tree](assets/data_mining_feature_selection_decision_tree.png)

---

### Feature Creation

Is another way to reduce dimensions by merging many features into new one feature

<!--prettier-ignore-->
!!! example "Example"
    We have two features **Width** and **Height** but we need **Area** in our computations, so we merge them into new feature **Area=Width*Height**

---

## Numerosity Reduction

Reducing Data Objects

### Parametric

DataSet is match with a **parametric model**, so train the **model** based on DataSet then remove DataSet

<!--prettier-ignore-->
!!! tip "Training"
    Computing the **Model** based on **DataSet**, then save **model parameters** and remove DataSet

    * DataSet => `ax^2 + bx + c` => **a, b, c**

There are some ways for parametric reduction:

1. Regression

---

#### Regression

Using regression we can find a **Function** that best matches our DataSet, then we can save this model instead of DataSet

---

### Non-Parametric

**Remove** some **record** from DataSet

There are some ways for non-parametric reduction:

1. Regression
2. Histogram
3. Clustering
4. Sampling
5. Stratified Sampling
6. Data Cube Aggregation

---

#### Regression

We can remove some point with **small error** from the regression line

---

#### Histogram

We can **binning** DataSet, then save **Buckets Mean** with **Bucket Frequency** instead of all values

---

#### Clustering

We can **clustering** DataSet, then save **Cluster Mean** with **Cluster Frequency** instead of all values

---

#### Simple Sampling

We can select a **SampleSet** from DataSet **randomly**, and remove SampleSet

<!--prettier-ignore-->
!!! tip "Simple Random Sample Without Replacement (SRSWOR)"
    Select item and don't replace it in basket

    * In the next selections this item will not occure

<!--prettier-ignore-->
!!! tip "Simple Random Sample With Replacement (SRSWR)"
    Select item and replace it in basket

    * In the next selections this item may occure

---

#### Stratified Sampling

When we have **Label** for DataObjects we can **Categorize** our data, then select **SampleSet** from each category based on **Category Frequency** randomly

```code
Label1 => [A,B,C,D] =>  Select 2 items
Label2 => [X,Y] =>      Select 1 items
```

---

#### Data Cube Aggregation

Aggregate DataSet using **OLAP** Data Integration into **DataCube**

---

## Compression Reduction

Compression binary data such as **Image** or **Audio** or **Video** or etc

---
