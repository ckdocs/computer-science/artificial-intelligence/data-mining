# Data Warehouse

A database used for data mining and analysis, it kept in a separate place that production database

<!--prettier-ignore-->
!!! tip "Why we use data warehouse?"
    For multiple reasons we create seperated database as data warehouse:

    1. Schemas are optimized for analysis (Only save analysable data)
    2. Save all old data (History analysis usage)
    3. Contains integrated data
    4. Contains general and partial data (Based on our data mining data needs)
       * Database(Day1, Day2, Day3, ...) => DWH(Month)

Is an integrated database with 4 attributes:

1. Subject oriented
2. Integrated
3. Time variant
4. Nonvolatile

## Subject Oriented

1. Only the data we **need for data mining** is stored in data warehouse
    - Save only columns that we need
2. Our goal is our analysis
    - Don't save the details

<!--prettier-ignore-->
!!! example "Example"
    We only want to mine about sells to offer customers

        ```code
        production
        TABLE User(id, name, age, username, password)

        data warehouse:
        TABLE User(name, age)
        TABLE Sells(cost, time, user)
        ```

---

## Integrated

The data warehouse data may be integrated from multiple datasources like **MySQL**, **SQLServer**, **File**, **API**, ...

For integrationg our data we must do 2 things:

1. Data Cleaning
2. Data Integration

---

## Time variant

The data stored in data warehouse may be very old like (10 years) because we need them for our analysis, but in out production database we may drop more that 1 years old data for performance

---

## Nonvolatile

We dont update our records in data warehouse they are immutable

1. Transactions may commit in the low traffic times (Amar system) (Loading of data)
2. Access to data (Read of data)

Operational Database: CRUD (OLTP(ER) - Online Transaction Processing - Transactions (Create, Update, Delete))
Analytical Datawarehouse: LR (OLAP(CUBE) - Online Analytical Processing - Analysis (Load, Read))

---

## Architecture

1. OLTP Servers (Operational Database) (SQLServer, MySQL)
2. ETL Tools (Kafka, Spark)
3. Data warehouse (Analytical Database) (Cube Storage) (HBase, Cassandra, ...)
4. OLAP Servers (Apache Kylin, Apache Spark)
5. Frontend (Metabase, PowerBI, ...) => Chart, Report, ...

![Architecture](assets/datawarehouse_architecture.jpg)
![Architecture](assets/datawarehouse_architecture.png)

---

### ETL (Migrate, Integrate)

1. Extract
2. Transform
3. Load

---

### OLAP vs OLTP

-   OLTP:
    -   Supports SQL (Structured Query Language)
    -   Database - Table - Column
    -   Commands: INSERT, SELECT, UPDATE, DELETE
-   OLAP:
    -   Supports MDX (Multidimensional Expressions)
    -   Datacube - Table - Measure - Dimension
    -   Commands: ROLLUP, DRILLDOWN, SLICE, DICE, PIVOT

---

### OLAP Schemas

In all OLAP schemas we have two types of tables:

1. Fact Table (Contains measures)
2. Dimension Table (Related to Fact Table)

#### Star Schema

One **Fact Table** and multiple **Dimension Tables** with one level of normalization

![Star](assets/datawarehouse_star.png)

#### Snowflake Schema

One **Fact Table** and multiple **Dimension Tables** with two level of normalization

![Snowflake](assets/datawarehouse_snowflake.png)

#### Fact Consellations Schema (General)

Multiple **Fact Table** and multiple **Dimension Tables** with n level of normalization

![Consellations](assets/datawarehouse_consellations.png)

---

## Example

1. Summarization
    1. Find **measure attributes** (We want to aggregate)
    2. Find **dimension attributes** (We aggregate based on them)
    3. Find **Cross-Tab (2D)** or **Cross-Tabulation (2D)** or **Pivot-Table (2D)** or **Data Cube (3D, nD)**

---

### Summarization

We have an operational database with this schema, and we want to suggest clothes to customers:

```code
TABLE Sales(
    name: skirt | dress | shirt | pant
    color: dark | pastel | white
    size: small | medium | large
    count: number
)
```

<!--prettier-ignore-->
!!! tip "Data Cube"
    Is a table that shows the aggregated **measure attribute** by any two or three or n numbers of **dimension attributes**

    * Each cell container **aggregation value** of dimensions
    * For **n** dimensions we find **2^n** aggregations

-   Our **Measure Attribute** is **Count** because the highe count is better for customers
-   Our **Dimension Attributes** are **Name**, **Color**, **Size** and we aggregate **count** by them

![Cross-Tab](assets/cross-tab-1.png)
![Cross-Tab](assets/cross-tab-2.png)

---

### Hierarchical

We have hierachy in our data models (nested models)

![Hierachy](assets/datawarehouse_hierachy.png)

---

## Data Integration

Integrate two database or table

Data integration problems:

1. Incompatibility (table1 -> id: string, table2 -> id: number => merge conflict)
2. Repetition (one user in two tables => merge duplication) (CCS data duplication in Receipt, BL)

---
