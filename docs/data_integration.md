# Data Integration

In this step we learn how to **integrate** our data from multiple **DataSources** into one **DataSource**:

Integration is done in two phases and in each phase we have some problems:

1. **Schema** Integration
    1. Entity Identification Problem: Different enums
        - Table1: ABC, Table2: 123 => Migrate
    2. Incompatibility: Different units
        - Table1: 12cm, Table2: 1.2dm => Migrate
2. **Data** Integration
    1. Data Object Duplication: Select correct record
        - Select correct one (similarity - regression - clustering)
    2. Data Attribute Duplication: Remove feature by finding correlation to another feature
        - Two features are equal in two tables (Username in two tables)
        - Two features are a function of them in two tables (Remove someone) (MonthlyIncome - TotalIncome)
        - Qualitative: Chi-2
        - Quantitative: Correlation

---

## Schema Integration

We want to integrate our DataSets Schemas into one Schema

---

### Entity Identification Problem

When two **columns** in two **DataSet** has equal value but they values are in different measures

Using a migration script, we can integrate and merge these problems

$$
\begin{aligned}
    & DataSet1: \texttt{ABC}
    \\
    & DataSet2: \texttt{123}
\end{aligned}
$$

---

### Incompatibility

When two **columns** in two **DataSet** has equal value but they values are in different units

Using a migration script, we can integrate and merge these problems

$$
\begin{aligned}
    & DataSet1: \texttt{12cm}
    \\
    & DataSet2: \texttt{1.2dm}
\end{aligned}
$$

---

## Data Integration

We want to integrate our DataSets into one DataSet

---

### Data Object Duplication (Record)

Two records can be duplicated, for example two user record in two table:

1. Customers: Name=nina, Age=12, Country=Yale
2. Users: Name=nina, Age=21, Country=Yale

For integrating databases we must remove this duplications by selecting correct one

---

### Data Attribute Duplication (Column)

Two columns can be correlated, using tests we can find correlation and prevent duplication:

1. **Qualitative Data**: Chi-2 Test for independence
    - Expected Frequency
    - Observed Frequency
2. **Quantitative Data**: Correlation Coefficients

---
