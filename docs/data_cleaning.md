# Data Cleaning

In this step we learn how to **filter** and **fix** our data in these problems:

1. Incomplete (Fix **missed** values)
2. Noisy - Outlier (Fix **noise** and **outlier**)
3. Inconsistent (Fix **metadata** compatibility)
4. Intentional (Check **fake** data)

<!--prettier-ignore-->
!!! tip "Reasons"
    There are many reasons for these problems:

    1. Input error
    2. Human error
    3. Computer error
    4. Transport error

---

## Incomplete (Miss)

Some **attributes** of data object that we needed are **missed** by these problems:

1. User not filled
2. Not exists in dataset (input is not exists in form)
3. Compound (Firstname, Lastname merged into Name - We save name in dataset)

<!--prettier-ignore-->
!!! example "Example"
    Person dont fill some inputs (job)

        ```js
        dataObject = {
            firstName: "Kazem",
            lastName: "Kazemi",
            birthDate: "1998-01-01"
            job: "",
        };
        ```

---

### Detecting

By scanning **DataSet** we can find **Missed** values, attributes that are **null**

---

### Handling

1. Ignore record
    - If dataobject hasn't label (class), remove it (cannot fill using other class record)
2. Fill human
3. Fill automate
    - A fixed value like null (classes: red, blue, gree, null)
    - Dataset mean
        - Normal dataset: **mean**
        - Skewed dataset: **median**
        - **Example**: physics score (attribute) => mean of all students physics
    - Class mean (Smarter way)
        - Normal dataset: **class mean**
        - Skewed dataset: **class median**
        - **Example**: physics score (attribute) => mean of good students class physics
4. Data mining (get from generated model)

---

## Noisy - Outlier

Some attributes of data object may be correct or **outlier**, or may be **noise**:

1. Incorrect data or **noise** filled (Input error, Human error)
    - Name: 11551aaadad
    - Birthdate: fffSAa
2. Correct but **outlier** data
    - Height: 3m
    - Age: 100

<!--prettier-ignore-->
!!! example "Example"
    Some values are **noise** or **outlier**

    1. Name: 11141313aaa (Noise)
    2. Birthdate: 121331dsd (Noise)
    3. Height: 3m (Outlier)
    4. Age: 100 (Outlier)

---

### Detecting

1. Binning
2. Regression
3. Clustering
4. Human

---

### Handling

1. Ignore record
    - Drop the data object from dataset
    - Ignore data attribute in computations
2. Binning
    - Equal Frequency binning by mean
    - Equal Frequency binning by borders median
3. Regression
    - Change data attribute noise value by regression estimate
4. Clustering
    - Change data attribute noise value by cluster mean estimate
5. Change by human

<!--prettier-ignore-->
!!! tip "Binning"
    1. Sort dataset
    2. Divide dataset into **Bins** or **Buckets** (equal frequency)
    3. Smoothing with:
        1. **Mean**: Convert bin values to bin **mean**
        2. **Borders median**: Convert between borders values to the **closer border**

    ```js
    DataSet = [4, 8, 9, 15, 21, 21, 24, 25, 26, 28, 29, 34];

    Bin1 = [4, 8, 9, 15];
    Bin2 = [21, 21, 24, 25];
    Bin3 = [26, 28, 29, 34];
    ```

<!--prettier-ignore-->
!!! tip "Regression"
    Change noise values into regression estimation:

    ![Regression](assets/linear_regression.png)

<!--prettier-ignore-->
!!! tip "Clustering"
    Find data object cluster and change noise values into cluster mean:

    ![Clustering](assets/clustering.png)

<!--prettier-ignore-->
!!! tip "Human"
    **Detect** noise or outlier values by **computer** and **handle** by **human**

---

## Incoinsistent (Metadata)

Some attributes of data object may be **incompatible** with **metadata** rules:

1. Relation incoinsistency
    - Age: 42, Birthdate: 2001-01-01
2. Integration incoinsistency
    - Old table column type: ABC, New table column type: 123
3. Duplication incoinsistency
    - User in table 1: age=20, User in table 2: age=12

<!--prettier-ignore-->
!!! example "Example"
    1. Username: 123 (not matches regex)
    2. Email: 1111333 (not matches regex)

---

### Detecting

1. Metadata
    - Loopback models, regex patterns
2. Unique
    - Loopback models, unique property
3. Nullable
    - Loopback models, nullable property
4. Advanced Tools
    - Regression
    - Clustering
    - Decision Tree
    - etc

---

## Intentional (Fake)

Some attributes of data object may be filled **fake** by user:

1. User enters incorrect data deliberate
2. User dont enter the input

<!--prettier-ignore-->
!!! tip "Null Values"
    We must save **Null Values** to determine whether user enters an input or not

---
