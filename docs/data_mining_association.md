# Association Rules

This data mining technique helps to discover a link between two or more items. It finds a hidden pattern in the data set.

Association rules are if-then statements that support to show the probability of interactions between data items within large data sets in different types of databases. Association rule mining has several applications and is commonly used to help sales correlations in data or medical data sets.

The way the algorithm works is that you have various data, For example, a list of grocery items that you have been buying for the last six months. It calculates a percentage of items being purchased together.

These are three major measurements technique:

Lift:
This measurement technique measures the accuracy of the confidence over how often item B is purchased.
(Confidence) / (item B)/ (Entire dataset)
Support:
This measurement technique measures how often multiple items are purchased and compared it to the overall dataset.
(Item A + Item B) / (Entire dataset)
Confidence:
This measurement technique measures how often item B is purchased when item A is purchased as well.
(Item A + Item B)/ (Item A)

# Data Mining

1. Find Frequent ItemSets
    - Greedy
    - Apriori
    - FPGrowth
    - ECLAT
2. Generate AssociativeRules

## Frequent Pattern

A pattern that frequently repeats in our dataset

<!--prettier-ignore-->
!!! tip "Pattern"
    Pattern is a subset of our dataset:

    1. A group of data objects (Milk, Bread, ...)
    2. A sequence of data objects (Camera -> SDCard)
    3. A structure like graph or tree (Relation between data objects)

Usages:

1. Find dataset patterns (relations between objects)
2. How many items, customers buy with each (Bread, Milk, Egg, ...)
3. What is the next customer purchase (Camera -> SDCard -> Cover)
4. Which types of DNA's are sensitive to this new medicine
5. Classify documents automatically

Example:

1. Basket analysis
2. Marketing
3. Catalog design
4. Blog analysis (Click analyse - User clicks in site)

Why?:

1. Assosiative rules analysis
2. Correlation analysis
3. Causality analysis

---

## Transactional DataSet

---

## ItemSet

Set of items that occur together

<!--prettier-ignore-->
!!! tip "K-ItemSet"
    An itemSet with **K** items

<!--prettier-ignore-->
!!! danger "Problem"
    Imagine we have a **Frequent ItemSet** with size **100 Items**, what is the number of **Frequent K-ItemSets** with **K < 100** ?

    $$
    \begin{aligned}
        & {100 \choose 1} + {100 \choose 2} + \dots + {100 \choose 100} = 2^{100} - 1 = 1.27 * 10^{30}
    \end{aligned}
    $$

    For generating these itemSets we have **Memory**, **Speed** problems, so we use **Closed Frequent ItemSet** and **Maximal Frequent ItemSet**

<!--prettier-ignore-->
!!! tip "Maximal vs Closed vs Frequent"
    The relation betwee **maximal frequent itemset**, **closed frequent itemset**, **frequent itemset** is a nested relation:
    
    ![Maximal vs Closed vs Frequent](assets/maximal_closed_frequent_itemset.png)

![ItemSet](assets/itemset.png)

---

## Lattice

The graph of all ItemSets

---

### Support (A, B)

The probability of occurance of itemSet in dataSet called **Support** (Transactional DataSet) (**Probability**)

-   **Absolute Support**: Number of dataObjects contains this itemSet (**ItemSet Frequency**)
-   **Relative Support**: Number of dataObjects contains this itemSet over dataSet size

$$
\begin{aligned}
    & Support(\{A,B,C\})
    \\
    & = P(\{A,B,C\})
    \\
    & = P(A \cup B \cup C)
    \\
    \\
    & = \frac{\texttt{Number of dataObjects contains A and B and C}}{N}
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Minimum Support"
    Is a constant that is defined by **DataMiner**, every ItemSet with support greater or equal to **Min-Support** is **Strong**

<!--prettier-ignore-->
!!! example "Example"
    This Transactional DataSet is a purchase dataset of a market:

    ![Transactional DataSet](assets/transactional_dataset.png)

    $$
    \begin{aligned}
        & Support(\{\}) = \frac{5}{5}
        \\
        \\
        & Support(\{Eggs\}) = \frac{3}{5}
        && Support(\{Nuts\}) = \frac{3}{5}
        \\
        & Support(\{Milk\}) = \frac{2}{5}
        && Support(\{Coke\}) = \frac{3}{5}
        \\
        & Support(\{Diaper\}) = \frac{4}{5}
        && Support(\{Coffee\}) = \frac{1}{5}
        \\
        \\
        & Support(\{Eggs, Nuts\}) = \frac{2}{5}
        && Support(\{Eggs, Milk\}) = \frac{2}{5}
        \\
        \\
        & Support(\{Eggs, Nuts, Milk\}) = \frac{1}{5}
        && Support(\{Eggs, Coke, Milk\}) = \frac{0}{5}
    \end{aligned}
    $$

---

### Confidence (A => B)

The probability of occurance of itemSet then occurance of another itemSet in dataSet called **Confidence** (**Conditional Probability**)

$$
\begin{aligned}
    & Confidence(\{A,B\} \Rightarrow \{C,D\})
    \\
    & = P(\{C,D\} | \{A,B\})
    \\
    & = \frac{Support(\{A,B,C,D\})}{Support(\{A,B\})}
    \\
    \\
    & = \frac{\texttt{Number of dataObjects contains A and B and C and D}}{\texttt{Number of dataObjects contains A and B}}
\end{aligned}
$$

<!--prettier-ignore-->
!!! tip "Minimum Confidence"
    Is a constant that is defined by **DataMiner**, every Rule with confidence greater or equal to **Min-Confidence** is **Strong**

<!--prettier-ignore-->
!!! example "Example"
    This Transactional DataSet is a purchase dataset of a market:

    ![Transactional DataSet](assets/transactional_dataset.png)

    $$
    \begin{aligned}
        & Confidence(\{\} \Rightarrow \{\}) = \frac{5}{5}
        \\
        \\
        & Confidence(\{Eggs\} \Rightarrow \{Nuts\}) = \frac{2}{3}
        \\
        \\
        & Confidence(\{Eggs, Milk\} \Rightarrow \{Coke\}) = \frac{1}{3}
    \end{aligned}
    $$

---

### Downward Closure

Subsets of and ItemSet have support **greater or equal** than that ItemSet support

```code
ItemSet = [A, B, C] (S=60%)

[A,B] => (S>=60%)
[A,C] => (S>=60%)
[B,C] => (S>=60%)

[A] => (S>=60%)
[B] => (S>=60%)
[C] => (S>=60%)
```

---

### Super ItemSet (SuperSet)

We have a **k-itemSet**, if we add an item to this item set, now we have **k+1-itemSet**, we called these, **Super ItemSets**

$$
\begin{aligned}
    & SuperSets(c) = \{ac, bd, dc\}
    \\
    & SuperSets(cd) = \{acd, bcd\}
\end{aligned}
$$

![Super ItemSet](assets/frequent_itemset.png)

---

### Frequent ItemSet

Is an ItemSet whose support is greater than **Min-Support**

$$
\begin{aligned}
    & FI^K = \{ X: ItemSet \mid supp(X) \gt MinSupport \}
    \\
    \\
    & FI^{1} = \{A, B, C, D\}
    \\
    & FI^{2} = \{AB, AD, BD, CD\}
    \\
    & FI^{1} = \{ABD\}
\end{aligned}
$$

![Frequent ItemSet](assets/frequent_itemset.png)

---

### Closed Frequent ItemSet

A Frequent ItemSet that **none** of that **SuperSets**, have not **Greater or Equal Support** (They may be frequent)

$$
\begin{aligned}
    & CFI^K = \{ X: ItemSet \mid Y \in SuperSets(X) \: : \: supp(X) \gt supp(Y) \}
    \\
    \\
    & CFI^{1} = \{A, C\}
    \\
    & CFI^{2} = \{CD\}
    \\
    & CFI^{1} = \{ABD\}
\end{aligned}
$$

![Closed Frequent ItemSet](assets/frequent_itemset.png)

---

### Maximal Frequent ItemSet

A Frequent ItemSet that **none** of that **SuperSets**, are not **Frequent**

$$
\begin{aligned}
    & MFI^K = \{ X: ItemSet \mid Y \in SuperSets(X) \: : Y \neq Frequent \}
    \\
    \\
    & MFI^{1} = \{\}
    \\
    & MFI^{2} = \{CD\}
    \\
    & MFI^{1} = \{ABD\}
\end{aligned}
$$

![Maximal Frequent ItemSet](assets/frequent_itemset.png)

---

## Association Rule (X => Y)

A sequence of **ItemSets** called association rule

Associative rule tells us customer buys **X** then buys **Y** (X => Y) with these probabilities:

$$
\begin{aligned}
    & Rule: X \Longrightarrow Y
    \\
    & X,Y \in ItemSet
\end{aligned}
$$

![Associative Rule](assets/associative_rule.png)

<!--prettier-ignore-->
!!! example "Example"
    We have the dataset of customers baskets, for example we know 90% **bread** customers, also buy **milk**, so we put bread and milk near themself, or be add some offer to a related item

    -   By finding the relation between items:
        1. We can increase the sales by suggesting items
        2. We can increase the sales by offering items

    ![Basket](assets/basket_analysis.png)

    By analysing the baskets we find the **Association Rules**

---

### ItemSet to Rule

We convert each **Frequent ItemSets** into multiple rule (Combinations of ItemSets):

```code
Frequent ItemSet = [Coke, Diaper, Milk]

Rule1: {} -> {Coke, Milk, Diaper}   (S=60%, C=100%)

Rule2: {Coke} -> {Milk, Diaper}     (S=60%, C=100%)
Rule3: {Milk} -> {Coke, Diaper}     (S=60%, C=100%)
Rule4: {Diaper} -> {Coke, Milk}     (S=60%, C=100%)

Rule5: {Coke, Milk} -> {Diaper}     (S=60%, C=100%)
Rule6: {Coke, Diaper} -> {Milk}     (S=60%, C=100%)
Rule7: {Milk, Diaper} -> {Coke}     (S=60%, C=100%)

Rule8: {Coke, Milk, Diaper} -> {}   (S=60%, C=100%)
```

---

### Strong Rule

Every rule with two conditions:

1. **Support** > **Min-Support**
2. **Confidence** > **Min-Confidence**

<!--prettier-ignore-->
!!! warning "Min Tunning"
    We must define **Min** values by hand !

    * If **Min was too low** => Then every rule is suggested (incorrect)
    * If **Min was too high** => No rule is suggested (incorrect)

    So the **Min** value **tuning** is so important

---

## Find Frequent ItemSets

### Greedy

1. Find all ItemSets
2. For all ItemSets search frequency in DataSet

---

There are three main algorithms used to find Frequent ItemSets

### Apriori (BFS - Breadth First Search)

Generate **Candidates** sets, and test them

Problem:

1. Generated candidates may be a lot
2. A lot of tests needed

Algorithm:

1. Find 1-ItemSets by scanning database
2. Self Join $L_K = L_{K-1} * L_{K-1}$
3. Filter ItemSets with size = K
4. Scan DataSet to find ItemSets Support (Low Speed)
5. Filter ItemSets with Support >= Min-Support

#### Extensions

1. Hash Based (K, K+1 in same time)
    1. Select all Transactions
    2. For each Transaction
        1. Find K-ItemSet **Support**
        2. Create combinations **K+1-ItemSets**
        3. Find **K+1-ItemSet** frequency using **HashTable**
2. Transaction Reduction (Filter transactions)
    1. If no contains any **K-ItemSet**
        1. cannot contain any **K+1-ItemSet**
3. Partitioning (When dataset is too large to load into RAM, we load it partitionary)
    1. Paritioning
    2. Find **Partition-Min-Support** (If DataSet has Normal Distribution => $\texttt{Partition Min Support} = \frac{\texttt{Min Support}}{\texttt{Number of partitions}}$)
    3. Find **Parition-Frequent-ItemSets**
    4. For all **Parition-Frequent-ItemSets** (Candidates) scan all database and find real **Support**
    5. Filter candidates by real **Support** and **Min-Support**
4. Sampling (When transactions are same)
    1. Get a SampleSet from DataSet
    2. Find strong rules based on **SampleSet**
5. Dynamic ItemSet Counting (DIC) (Add another filter after step 2 in apriori - before scan)
    1. Filter K-ItemSets that !(All (K-1)-ItemSets subsets are frequent)

---

### FPGrowth (DFS - Depth First Search)

1. It compresses the DataSet into **FP-Tree** structure
2. Then it will find support of **1-ItemSets** by summing nodes frequencies
3.

4) Generate **FP-Tree**
    1. Find 1-ItemSets by scanning database
    2. Sort 1-ItemSets by support
    3. For each Transaction
        1. Read Transaction items
        2. Sort Transaction items by 1-ItemSets order
        3. Generate FP-Tree in depth of Transaction items order
            1. If (child not exist), add and set support=1
            2. Else add 1 to support
            3. Then go to next Transaction item
5) Generate **Conditional FP-Trees**
    1. Per every item find **Conditional Pattern Base** in **FP-Tree Leaf Paths**
    2. Convert items **Conditional Pattern Base** into **Conditional FP-Tree**
    3. ?

---

### ECLAT

---

## Generate AssociationRules

For each Frequent ItemSet:

1. Find all subsets (0,1,2,...,n)
2. For each subset-complement generate **Rule**, **Reverse-Rule** as **AssociationRule**
3. For each association rule find **Confidence** by $\textbf{L}_i$ tables
4. Filter strong rules

---
