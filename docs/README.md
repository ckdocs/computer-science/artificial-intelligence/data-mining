# Data Mining

The technique used to **extract** valuable information from **huge sets of data** called **Data
Mining** or **Knowledge Discovery in Database** (KDD)

![Data Mining](assets/data_mining.png)

<!--prettier-ignore-->
!!! tip "History"
    The data mining history is about their algorithms and concepts:

    1. Bayes theorem (1700s)
    2. Regression (1800s)
    3. Clustering (1950s)
    4. Neural networks (1950s)
    5. Genetic algorithms (1950s)
    6. Decision trees (1960s)
    7. Supporting vector machines (1990s)

<!--prettier-ignore-->
!!! tip "KDD Process"
    Extracting **valuable information** from DataSources consists of multiple **steps**:

    1. Domain Understanding
    2. Data Cleaning
    3. Data Integration
    4. Data Selection (Reduction)
    5. Data Transformation
    6. Data Mining
        1. Classification
        2. Clustering
        3. Regression
        4. Outlier Analysis
        5. Sequential Patterns
        6. Association Rules
    7. Pattern Evaluation
    8. Knowledge Presentation

    ![Data Mining Steps](assets/data_mining_steps.png)

---

## Data Quality

Before performing data mining algorithms we need to check our data quality

The good data has some features:

1. **Accuracy**:
    - Data should not have **outlier** or **noise**
2. **Completeness**:
    - Data should not have **missed** values
3. **Consistency**:
    - Data should match to it's **metadata patterns** (Username regex, ...)
4. **Timeliness**:
    - Data should be **updated without delay** (Correct time)
5. **Believability**:
    - Data should not be **fake** (We believe the data)
6. **Interpretability**:
    - Data should be **clear** (Not encrypted)

<!--prettier-ignore-->
!!! tip "Data Preprocessing"
    For reaching these goals and preparing a data with these qualities, we must do 4 things:

    1. Data Cleaning
    2. Data Integration
    3. Data Selection
    4. Data Transformation

---

## Data Sources

Data mining can perform in these data sources:

1. Relational Database (Relation)
2. Data Warehouse (Integration)
3. Object-Relational Database (Extend)
4. Transactional Database (Transaction)

---

### Relational Database

A collection of multiple **DataSets** named as **Tables** that are related

-   **DataSet**: Table
-   **DataObject**: Record
-   **DataAttribute**: Column

![Relational Database](assets/data_mining_relationaldatabase.png)

---

### Data Warehouse

A datasource used for **Integrated** data, it will **aggregates** data in **dimensions**

1. **DataSet**: Cube
2. **DataObject**: Dimension
3. **DataAttribute**: Cell

![Data Warehouse](assets/data_mining_datawarehouse.png)

---

### Object-Relational Database

A datasource, that supports **Inheritance**

1. **DataSet**: Collection
2. **DataObject**: Class
3. **DataAttribute**: Property

![Object-Relational Database](assets/data_mining_objectrelationaldatabase.png)

---

### Transactional Database

A datasource, that column values can be **Array** or a **Set of transactions**

1. **DataSet**: Table
2. **DataObject**: Transaction
3. **DataAttribute**: Transaction Items

![Transactional Database](assets/data_mining_transactionaldatabase.png)

---

## Tools

Is a framework that implemented the **Data Mining** algorithms, and we can **load** our data into the framework, then apply **algorithms** like classification, clustering, ... on it

There are two types of tools used for data mining:

1. Text Interfaces (Programming)
    1. Tensorflow (Python, JS, Java, C++)
    2. Keras (Python)
    3. Spark (Java, API)
    4. Matlab-Octave (Matlab)
2. Graphical Interfaces (Drag-Drop)
    1. Weka (Java based)
    2. RapidMiner (Java based)
    3. KNIME (Java based)
    4. Orange (Python based)
    5. BigML (Cloud based)

![Tools](assets/data_mining_tools.jpg)

---
