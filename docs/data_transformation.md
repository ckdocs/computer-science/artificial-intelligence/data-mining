# Data Transformation

In this step we learn how to **transform** data in another **scale** or **format**

There are two types of **transformation**:

1. Normalization (Change scale)
2. Discretization (Break data)

<!--prettier-ignore-->
!!! tip "Reasons"
    There are many reasons for transforming data:

    1. Changing all attributes scales to be comparable
    2. Breaking data into SubSets to increase speed

<!--prettier-ignore-->
!!! tip "Other Transformations"
    In the past steps, we learn some other types of transformation:

    1. Data Cleaning => Smoothing (removing noise)
    2. Data Integration => Summarization (data cube aggregation)
    3. Data Selection => Feature Creation

---

## Normalization

Change Data **Scale** and **Mean**

There are three main types of normalization:

1. Min-Max Normalization
2. Z-Score Normalization
3. Decimal Normalization

---

### Min-Max Normalization

Normalizing data in new **Min-Max** scale:

1. Transform => 0
2. Scale => Max-Min
3. Transform => Mean

$$
\begin{aligned}
    & A = V - Min_{old} && \texttt{Transform}
    \\
    & B = \frac{A}{Max_{old} - Min_{old}} && \texttt{Scale}
    \\
    & C = B . (Max_{new} - Min_{new}) && \texttt{Scale}
    \\
    & V' = C + Min_{new} && \texttt{Transform}
    \\
    \\
    & V' = \frac{V - Min_{old}}{Max_{old} - Min_{old}} (Max_{new} - Min_{new}) + Min_{new}
\end{aligned}
$$

![Min-Max Normalization](assets/data_mining_transformation_minmax_normalization.png)

---

### Z-Score Normalization

Normalizing data in new **Standard Normal** scale:

$$
\begin{aligned}
    & V' = \frac{V - \mu}{\sigma}
    \\
    \\
    & \mu: \texttt{Mean}
    \\
    & \sigma: \texttt{Standard Deviation}
\end{aligned}
$$

![Z-Score Normalization](assets/data_mining_transformation_zscore_normalization.png)

---

### Decimal Normalization

Normalizing data in new **[-1,1]** scale:

$$
\begin{aligned}
    & V' = \frac{V}{10^j}
    \\
    \\
    & j: Digits\{Max(|V \neq 100\dots0|)\}
\end{aligned}
$$

<!--prettier-ignore-->
!!! example "Example"
    $$
    \begin{aligned}
        & DataSet: \{-986, \dots, 9709\}
        \\
        & j = Digits(9709) = 4
        \\
        \\
        & NormalizedDataSet: \{\frac{-986}{10000}, \dots, \frac{9709}{10000}\} \in (-1, 1)
    \end{aligned}
    $$

---

## Discretization

Break Data into **SubSets**

There are seven main types of discretization:

1. Binning
2. Histogram Analysis
3. Clustering Analysis
4. Decision Tree Analysis
5. Correlation Analysis
6. Classification
7. Hierachical

---

### Binning

We can use **Binning** to discretize our data into **Buckets**

---

### Histogram Analysis

Using **Histogram** we can discretize our data into **Ranges**

---

### Clustering Analysis

Using **Clustering** we can discretize our data into **Clusters**

---

### Decision Tree Analysis

Using **Decision Tree** we can discretize our data into **Partitions**

---

### Correlation Analysis

Using **Correlation** we can merge **Correlated** data into one **Item**

---

### Classification

Using **Classification** we can discretize our data into **Classes**

---

### Hierachical

Using **Hierachical** we can discretize our data into **Trees**

---
